const DatabaseDriver = require('../classes/databaseDriver/BetterSQLite3');

describe('database driver ', () => {
  test('create db', async () => {
    const db = await DatabaseDriver.create(`${__dirname}/db/empty.sqlite`);
    expect(db.open).toBe(true);
  });
});
